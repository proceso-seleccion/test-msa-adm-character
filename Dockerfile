FROM gradle:7.6.4-jdk17-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:17-alpine
EXPOSE 8080
COPY --from=build /home/gradle/src/build/libs/*.jar /app/test-msa-adm-character-0.0.1.jar
WORKDIR /app
ENTRYPOINT ["java","-jar","test-msa-adm-character-0.0.1.jar"]
