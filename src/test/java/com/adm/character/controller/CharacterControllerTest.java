package com.adm.character.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.adm.character.service.CharacterService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@WebMvcTest(CharacterController.class)
public class CharacterControllerTest {

  @MockBean
  private CharacterService characterService;
  @Autowired
  private MockMvc mockMvc;

  @Test
  void shouldReturnResponseWhenCallCharactersGet() throws Exception {
    MvcResult mvcResult =
        this.mockMvc
            .perform(
                get("/api/v1/characters")
                    .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andDo(print())
            .andReturn();
    String responseBody = mvcResult.getResponse().getContentAsString();
    Assertions.assertThat(responseBody).isNotNull();
  }
  @Test
  void shouldReturnResponseWhenCallCharactersByIdGet() throws Exception {
    MvcResult mvcResult =
        this.mockMvc
            .perform(
                get("/api/v1/characters/1")

                    .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andDo(print())
            .andReturn();
    String responseBody = mvcResult.getResponse().getContentAsString();
    Assertions.assertThat(responseBody).isNotNull();
  }
}
