package com.adm.character.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.adm.character.factory.ParameterFactory;
import com.adm.character.service.UserService;
import com.adm.character.util.JsonUtil;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@WebMvcTest(UserController.class)
public class UserControllerTest {

  @MockBean
  private UserService userService;
  @Autowired
  private MockMvc mockMvc;

  @Test
  void shouldReturnResponseWhenCallCharactersGet() throws Exception {
    MvcResult mvcResult =
        this.mockMvc
            .perform(
                post("/api/v1/users")
                    .content(JsonUtil.transformObjectToJson(ParameterFactory.getUserDto()))
                    .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andDo(print())
            .andReturn();
    String responseBody = mvcResult.getResponse().getContentAsString();
    Assertions.assertThat(responseBody).isNotNull();
  }

  @Test
  void shouldReturnResponseWhenCallGetByEmailPasswd() throws Exception {
    MvcResult mvcResult =
        this.mockMvc
            .perform(
                get("/api/v1/users/c_santillan2008@hotmail.es/1234")

                    .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andDo(print())
            .andReturn();
    String responseBody = mvcResult.getResponse().getContentAsString();
    Assertions.assertThat(responseBody).isNotNull();
  }

  @Test
  void shouldReturnResponseWhenCallSaveUserCharacter() throws Exception {
    MvcResult mvcResult =
        this.mockMvc
            .perform(
                post("/api/v1/usersCharacters")
                    .content(JsonUtil.transformObjectToJson(ParameterFactory.getUserCharacterFavorite()))
                    .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andDo(print())
            .andReturn();
    String responseBody = mvcResult.getResponse().getContentAsString();
    Assertions.assertThat(responseBody).isNotNull();
  }

  @Test
  void shouldReturnResponseWhenCallGetUserCharacterByUser() throws Exception {
    MvcResult mvcResult =
        this.mockMvc
            .perform(
                get("/api/v1/usersCharacters/1234")

                    .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andDo(print())
            .andReturn();
    String responseBody = mvcResult.getResponse().getContentAsString();
    Assertions.assertThat(responseBody).isNotNull();
  }

  @Test
  void shouldReturnResponseWhenCallPatchUserCharacterById() throws Exception {
    MvcResult mvcResult =
        this.mockMvc
            .perform(
                patch("/api/v1/usersCharacters/1")
                    .content(JsonUtil.transformObjectToJson(ParameterFactory.getUserCharacterFavorite()))
                    .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andDo(print())
            .andReturn();
    String responseBody = mvcResult.getResponse().getContentAsString();
    Assertions.assertThat(responseBody).isNotNull();
  }

  @Test
  void shouldReturnExceptionWhenCallPatchUserCharacterById() throws Exception {
    MvcResult mvcResult =
        this.mockMvc
            .perform(
                patch("/api/v1/usersCharacters/1")
                    .content("{}")
                    .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isBadRequest())
            .andDo(print())
            .andReturn();
    String responseBody = mvcResult.getResponse().getContentAsString();
    Assertions.assertThat(responseBody).isNotNull();
  }
}
