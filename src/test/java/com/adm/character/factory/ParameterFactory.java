package com.adm.character.factory;

import com.adm.character.service.dto.CharacterDto;
import com.adm.character.service.dto.CharacterDto.ItemDto;
import com.adm.character.service.dto.UserCharacterFavoriteDto;
import com.adm.character.service.dto.UserDto;
import java.util.ArrayList;

public class ParameterFactory {

  private ParameterFactory() {
  }

  public static UserDto getUserDto() {
    return UserDto.builder().email("c_santillan2008@hotmail.es").password("12345")
        .name("Christian Santillan").status(Boolean.TRUE).build();
  }

  public static UserCharacterFavoriteDto getUserCharacterFavorite() {
    return UserCharacterFavoriteDto.builder().user(getUserDto()).idCharacter(1).character("{}")
        .status(Boolean.TRUE).build();
  }

  public static CharacterDto getCharacter() {

    return CharacterDto.builder().items(new ArrayList<>()).build();
  }
  public static ItemDto getItem() {
    ItemDto item = new ItemDto();
    item.setAffiliation("123");

    return item;
  }
}
