package com.adm.character.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.adm.character.domain.User;
import com.adm.character.domain.UserCharacterFavorite;
import com.adm.character.factory.ParameterFactory;
import com.adm.character.repository.UserCharacterFavoriteRepository;
import com.adm.character.repository.UserRepository;
import com.adm.character.service.impl.UserServiceImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
class UserServiceTest {

  @Mock
  private UserRepository userRepository;
  @Mock
  private UserCharacterFavoriteRepository userCharacterFavoriteRepository;
  @InjectMocks
  private UserServiceImpl userService;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void givenUserDtoWhenCallSaveReturnResponse() {
    User user = null;
    Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(user);
    assertThat(userService.save(ParameterFactory.getUserDto())).isNotNull();
    user = new User();
    Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(user);
    try {
      assertThat(userService.save(ParameterFactory.getUserDto())).isNotNull();
    } catch (Exception e) {
      assertThat(e).isNotNull();
    }
  }

  @Test
  void givenEmailPasswordWhenCallGetByEmailPasswordReturnResponse() {
    User user = new User();
    Mockito.when(userRepository.findByEmailAndPassword(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(user);
    assertThat(userService.getByEmailPassword("c_santillan2008@hotmail.es", "1234")).isNotNull();
    user = null;
    Mockito.when(userRepository.findByEmailAndPassword(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(user);
    try {
      assertThat(userService.getByEmailPassword("c_santillan2009@hotmail.es", "12343")).isNotNull();
    } catch (Exception e) {
      assertThat(e).isNotNull();
    }
  }

  @Test
  void givenUserCharacterFavoriteDtoWhenCallSaveReturnResponse() {

    UserCharacterFavorite userCharacterFavorite = null;
    Mockito.when(
        userCharacterFavoriteRepository.findByIdUserAndIdCharacterAndStatus(Mockito.anyInt(),
            Mockito.anyInt(), Mockito.anyBoolean())).thenReturn(userCharacterFavorite);
    assertThat(userService.saveCharactersFavorite(
        ParameterFactory.getUserCharacterFavorite())).isNotNull();

    userCharacterFavorite = new UserCharacterFavorite();
    Mockito.when(
        userCharacterFavoriteRepository.findByIdUserAndIdCharacterAndStatus(Mockito.anyInt(),
            Mockito.anyInt(), Mockito.anyBoolean())).thenReturn(userCharacterFavorite);

    try {
      assertThat(userService.saveCharactersFavorite(
          ParameterFactory.getUserCharacterFavorite())).isNotNull();
    } catch (Exception e) {
      assertThat(e).isNotNull();
    }
  }

  @Test
  void givenIdCharacterFavoriteDtoWhenCallUpdateReturnResponse() {
    UserCharacterFavorite userCharacterFavoriteEntity = new UserCharacterFavorite();
    userCharacterFavoriteEntity.setStatus(Boolean.TRUE);
    Optional<UserCharacterFavorite> optional = Optional.of(userCharacterFavoriteEntity);
    Mockito.when(
        userCharacterFavoriteRepository.findById(Mockito.anyInt()
        )).thenReturn(optional);
    assertThat(userService.updateCharactersFavorite(
        1, ParameterFactory.getUserCharacterFavorite())).isNotNull();
  }

  @Test
  void givenIdUserWhenCallGetCharactersFavoriteReturnResponse() {
    List<UserCharacterFavorite> userCharacterFavoriteList = new ArrayList<>();
    Mockito.when(
        userCharacterFavoriteRepository.findByIdUserAndStatus(Mockito.anyInt(), Mockito.anyBoolean()
        )).thenReturn(userCharacterFavoriteList);
    assertThat(userService.getCharactersFavoriteByIdUser(
        1)).isNotNull();
    userCharacterFavoriteList = null;
    Mockito.when(
        userCharacterFavoriteRepository.findByIdUserAndStatus(Mockito.anyInt(), Mockito.anyBoolean()
        )).thenReturn(userCharacterFavoriteList);
    try {
      assertThat(userService.getCharactersFavoriteByIdUser(
          1)).isNotNull();
    } catch (Exception e) {
      assertThat(e).isNotNull();
    }

  }
}
