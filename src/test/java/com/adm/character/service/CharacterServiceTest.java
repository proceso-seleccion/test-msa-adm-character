package com.adm.character.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.adm.character.factory.ParameterFactory;
import com.adm.character.repository.CharacterRepository;
import com.adm.character.service.dto.CharacterDto;
import com.adm.character.service.dto.CharacterDto.ItemDto;
import com.adm.character.service.impl.CharacterServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
class CharacterServiceTest {

  @Mock
  private CharacterRepository characterRepository;
  @InjectMocks
  private CharacterServiceImpl characterService;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void givenNoneWhenCallGetAllReturnResponse() {
    ResponseEntity<CharacterDto> response = new ResponseEntity<>(ParameterFactory.getCharacter(),
        HttpStatus.OK);
    Mockito.when(characterRepository.getAllCharacter()).thenReturn(response);
    assertThat(characterService.getAll()).isNotNull();
  }

  @Test
  void givenIdWhenCallGetByIdReturnResponse() {
    ResponseEntity<ItemDto> response = new ResponseEntity<>(ParameterFactory.getItem(),
        HttpStatus.OK);
    Mockito.when(characterRepository.getCharacterById(Mockito.anyInt())).thenReturn(response);
    assertThat(characterService.getById(1)).isNotNull();
  }
}
