package com.adm.character.util;

import com.adm.character.exception.AdmCharacterException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;

public class JsonUtil {

  private static ObjectMapper mapper = new ObjectMapper();

  private JsonUtil() {

  }

  public static String transformObjectToJson(Object object) throws AdmCharacterException {
    try {
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      return mapper.writeValueAsString(object);
    } catch (JsonProcessingException e) {
      throw new AdmCharacterException(
          "error", HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), null);
    }
  }
}
