package com.adm.character.exception;

import org.springframework.http.HttpStatus;

@lombok.Getter
public class AdmCharacterException extends RuntimeException {

  private final HttpStatus httpStatus;
  private final String backend;
  private final String type;

  public AdmCharacterException(String message, HttpStatus httpStatus, String backend, String type) {
    super(message);
    this.httpStatus = httpStatus;
    this.backend = backend;
    this.type = type;
  }

}
