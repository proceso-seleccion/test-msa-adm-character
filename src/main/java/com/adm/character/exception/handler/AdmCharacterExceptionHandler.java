package com.adm.character.exception.handler;


import com.adm.character.exception.AdmCharacterException;
import com.adm.character.exception.dto.ApiExceptionResponse;
import com.adm.character.exception.dto.ErrorCodeResponse;
import com.adm.character.util.Constant;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@lombok.extern.log4j.Log4j2
@lombok.RequiredArgsConstructor(access = lombok.AccessLevel.PROTECTED)
@lombok.experimental.FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
@ControllerAdvice
public class AdmCharacterExceptionHandler {


  @ExceptionHandler(MethodArgumentNotValidException.class)
  protected ResponseEntity<ApiExceptionResponse> handleMethodArgumentNotValid(
      MethodArgumentNotValidException exception) {
    ApiExceptionResponse responseError = getExceptionResponse(exception);
    return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(Exception.class)
  protected ResponseEntity<ApiExceptionResponse> handleException(
      Exception exception) {
    ApiExceptionResponse responseError = getExceptionResponse(exception);
    return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(AdmCharacterException.class)
  protected ResponseEntity<ApiExceptionResponse> handleFircGtwException(
      AdmCharacterException exception) {
    ApiExceptionResponse responseError = getExceptionResponse(exception);
    return new ResponseEntity<>(responseError, responseError.getErrors().get(0).getHttpStatus());
  }

  private ApiExceptionResponse getExceptionResponse(Exception exception) {
    ApiExceptionResponse responseError = new ApiExceptionResponse();
    responseError.setTitle("Adm character Error");
    responseError.setBackend("Adm character service");
    responseError.setDetail("Error in adm character service, see errors for details");

    List<ErrorCodeResponse> errorCodeResponseList = new ArrayList<>();
    responseError.setErrors(errorCodeResponseList);
    if (exception instanceof BindException) {
      BindException bindException = (BindException) exception;
      if (bindException.hasFieldErrors()) {
        bindException.getFieldErrors().forEach(field -> {
          ErrorCodeResponse error = new ErrorCodeResponse();
          error.setErrorCode(HttpStatus.BAD_REQUEST.value());
          error.setHttpStatus(HttpStatus.BAD_REQUEST);
          error.setErrorReason(field.getDefaultMessage());
          error.setErrorSource(field.getField());
          errorCodeResponseList.add(error);
        });
        log.error(Constant.EXCEPTION_BODY_ERROR, () -> exception.getClass().getSimpleName(),
            () -> responseError);
        return responseError;
      }
    }
    String source = exception.getMessage();
    ErrorCodeResponse error = new ErrorCodeResponse();
    if (exception instanceof AdmCharacterException) {
      source = ((AdmCharacterException) exception).getBackend();
      error.setErrorCode(((AdmCharacterException) exception).getHttpStatus().value());
      error.setHttpStatus(((AdmCharacterException) exception).getHttpStatus());
    } else {
      error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
      error.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    error.setErrorReason(exception.getMessage());
    error.setErrorSource(source);
    errorCodeResponseList.add(error);
    log.error(Constant.EXCEPTION_BODY_ERROR, () -> exception.getClass().getSimpleName(),
        () -> responseError);
    return responseError;
  }
}
