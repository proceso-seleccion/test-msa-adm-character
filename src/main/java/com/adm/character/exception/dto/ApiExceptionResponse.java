package com.adm.character.exception.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;

/** This class allows to manage exception properties */
@lombok.Data
@lombok.Builder
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.experimental.FieldDefaults(level = lombok.AccessLevel.PRIVATE)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiExceptionResponse {


  String title;
  String component;
  String detail;
  String resource;
  String backend;
  String type;
  List<ErrorCodeResponse> errors;
}
