package com.adm.character.repository;

import com.adm.character.domain.UserCharacterFavorite;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCharacterFavoriteRepository extends
    JpaRepository<UserCharacterFavorite, Integer> {

  UserCharacterFavorite findByIdUserAndIdCharacterAndStatus(Integer idUser, Integer idCharacter,
      Boolean status);

  List<UserCharacterFavorite> findByIdUserAndStatus(Integer idUser, Boolean status);


}
