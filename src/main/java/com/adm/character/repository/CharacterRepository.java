package com.adm.character.repository;

import com.adm.character.configuration.FeignClientDefaultSettings;
import com.adm.character.service.dto.CharacterDto;
import com.adm.character.service.dto.CharacterDto.ItemDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(
    name = "character-client",
    url = "${character.url}",
    configuration = FeignClientDefaultSettings.class)
public interface CharacterRepository {

  @GetMapping("${character.getAllCharacter}")
  ResponseEntity<CharacterDto> getAllCharacter();

  @GetMapping("${character.getOneCharacter}")
  ResponseEntity<ItemDto> getCharacterById(@PathVariable Integer id);

}
