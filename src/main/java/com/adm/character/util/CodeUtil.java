package com.adm.character.util;


import org.apache.commons.codec.digest.DigestUtils;

public class CodeUtil {

  public static String encrypt(String password) {
    return DigestUtils.sha512Hex(password);
  }

}
