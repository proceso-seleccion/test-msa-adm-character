package com.adm.character.configuration;

import org.springframework.context.annotation.Bean;


public class FeignClientDefaultSettings {

  @Bean
  public FeignClientDefaultInterceptor feignClientDefaultInterceptor() {
    return new FeignClientDefaultInterceptor();
  }

}
