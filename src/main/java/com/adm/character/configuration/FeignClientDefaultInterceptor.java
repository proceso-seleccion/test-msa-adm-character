package com.adm.character.configuration;

import feign.RequestInterceptor;
import feign.RequestTemplate;


public class FeignClientDefaultInterceptor implements RequestInterceptor {

  @Override
  public void apply(RequestTemplate template) {
    //template.header("Content-Type", "application/json");
    template.removeHeader("Content-Length");
  }


}
