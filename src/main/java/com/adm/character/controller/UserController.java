package com.adm.character.controller;

import com.adm.character.service.UserService;
import com.adm.character.service.dto.ResponseDto;
import com.adm.character.service.dto.UserCharacterFavoriteDto;
import com.adm.character.service.dto.UserDto;
import jakarta.validation.Valid;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@lombok.RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping(value = "${testadm.url}",
    produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@RestController
public class UserController {

  UserService userService;

  @PostMapping(value = "${testadm.user}")
  public ResponseEntity<ResponseDto> save(
      @Valid @RequestBody UserDto user) {
    log.info("Start method save :: Parameters [{}]", () -> user);
    ResponseDto response = userService.save(user);
    log.info("End method save :: Result [{}]", () -> response);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @GetMapping(value = "${testadm.userEmailPasswd}")
  public ResponseEntity<ResponseDto> getByEmailPasswd(
      @PathVariable String email, @PathVariable String passwd) {
    log.info("Start method getByEmailPasswd :: Parameters [{},{}]", () -> email, () -> passwd);
    ResponseDto response = userService.getByEmailPassword(email, passwd);
    log.info("End method getByEmailPasswd :: Result [{}]", () -> response);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @PostMapping(value = "${testadm.userCharacter}")
  public ResponseEntity<ResponseDto> saveUserCharacter(
      @Valid @RequestBody UserCharacterFavoriteDto userCharacterFavorite) {
    log.info("Start method saveUserCharacter :: Parameters [{}]", () -> userCharacterFavorite);
    ResponseDto response = userService.saveCharactersFavorite(userCharacterFavorite);
    log.info("End method saveUserCharacter :: Result [{}]", () -> response);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @GetMapping(value = "${testadm.userCharacterByUser}")
  public ResponseEntity<ResponseDto> getUserCharacterByUser(
      @PathVariable Integer idUser) {
    log.info("Start method getUserCharacterByUser :: Parameters [{}]", () -> idUser);
    ResponseDto response = userService.getCharactersFavoriteByIdUser(idUser);
    log.info("End method getUserCharacterByUser :: Result [{}]", () -> response);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @PatchMapping(value = "${testadm.userCharacterPatch}")
  public ResponseEntity<ResponseDto> patchUserCharacterById(
      @PathVariable Integer id,
      @Valid @RequestBody UserCharacterFavoriteDto userCharacterFavorite) {
    log.info("Start method patchUserCharacterById :: Parameters [{}]", () -> id);
    ResponseDto response = userService.updateCharactersFavorite(id, userCharacterFavorite);
    log.info("End method patchUserCharacterById :: Result [{}]", () -> response);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
