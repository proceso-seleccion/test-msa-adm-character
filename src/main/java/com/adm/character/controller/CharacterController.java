package com.adm.character.controller;

import com.adm.character.service.CharacterService;
import com.adm.character.service.dto.CharacterDto;
import com.adm.character.service.dto.CharacterDto.ItemDto;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@lombok.RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping(value = "${testadm.url}",
    produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class CharacterController {

  CharacterService characterService;

  @GetMapping(value = "${testadm.adm.getAll}")
  public ResponseEntity<CharacterDto> getCharacters() {
    log.info("Start method getCharacters");
    CharacterDto response = characterService.getAll();
    log.info("End method getCharacters :: Result [{}]", () -> response);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @GetMapping(value = "${testadm.adm.getOne}")
  public ResponseEntity<ItemDto> getCharacterById(@PathVariable Integer id) {
    log.info("Start method getCharacter :: Parameters [{}]", () -> id);
    ItemDto response = characterService.getById(id);
    log.info("End method getCharacters :: Result [{}]", () -> response);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
