package com.adm.character.service;

import com.adm.character.service.dto.CharacterDto;
import com.adm.character.service.dto.CharacterDto.ItemDto;
import java.util.List;

public interface CharacterService {

    CharacterDto getAll();
    ItemDto getById(Integer id);

}
