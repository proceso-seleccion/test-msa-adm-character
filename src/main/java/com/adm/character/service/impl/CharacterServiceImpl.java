package com.adm.character.service.impl;

import com.adm.character.repository.CharacterRepository;
import com.adm.character.service.CharacterService;
import com.adm.character.service.dto.CharacterDto;
import com.adm.character.service.dto.CharacterDto.ItemDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class CharacterServiceImpl implements CharacterService {

  CharacterRepository characterRepository;

  @Override
  public CharacterDto getAll() {
    return characterRepository.getAllCharacter().getBody();
  }

  @Override
  public ItemDto getById(Integer id) {
    return characterRepository.getCharacterById(id).getBody();
  }
}
