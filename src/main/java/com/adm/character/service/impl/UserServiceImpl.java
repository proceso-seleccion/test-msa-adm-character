package com.adm.character.service.impl;

import com.adm.character.domain.User;
import com.adm.character.domain.UserCharacterFavorite;
import com.adm.character.exception.AdmCharacterException;
import com.adm.character.repository.UserCharacterFavoriteRepository;
import com.adm.character.repository.UserRepository;
import com.adm.character.service.UserService;
import com.adm.character.service.dto.ResponseDto;
import com.adm.character.service.dto.UserCharacterFavoriteDto;
import com.adm.character.service.dto.UserDto;
import com.adm.character.service.mapper.UserCharacterFavoriteMapper;
import com.adm.character.service.mapper.UserMapper;
import com.adm.character.util.CodeUtil;
import com.adm.character.util.Constant;
import java.util.List;
import java.util.Objects;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Log4j2
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class UserServiceImpl implements UserService {

  UserRepository userRepository;
  UserCharacterFavoriteRepository userCharacterFavoriteRepository;


  @Override
  public ResponseDto save(UserDto user) {
    User userAux = userRepository.findByEmail(user.getEmail());
    if (Objects.isNull(userAux)) {
      User userEntity = UserMapper.MAPPER.mapUserDtoToUser(user);
      userEntity.setPassword(CodeUtil.encrypt(user.getPassword()));
      userRepository.save(userEntity);
      return ResponseDto.builder().message(Constant.OK).build();
    } else {
      throw new AdmCharacterException("User already exists", HttpStatus.BAD_REQUEST, null, null);
    }
  }


  @Override
  public ResponseDto getByEmailPassword(String email, String password) {
    User user = userRepository.findByEmailAndPassword(email, password);
    if (Objects.isNull(user)) {
      throw new AdmCharacterException("User not exists", HttpStatus.NOT_FOUND, null, null);
    } else {
      return ResponseDto.builder().message(Constant.OK)
          .user(UserMapper.MAPPER.mapUserToUserDto(user)).build();
    }
  }

  @Override
  public ResponseDto saveCharactersFavorite(UserCharacterFavoriteDto userCharacterFavorite) {
    UserCharacterFavorite userCharacterFavoriteAux = userCharacterFavoriteRepository.findByIdUserAndIdCharacterAndStatus(
        userCharacterFavorite.getUser().getId(), userCharacterFavorite.getIdCharacter(),
        Boolean.TRUE);
    if (Objects.isNull(userCharacterFavoriteAux)) {
      UserCharacterFavorite userCharacterFavoriteEntity = UserCharacterFavoriteMapper.MAPPER.mapUserCharacterFavoriteDtoToUserCharacterFavorite(
          userCharacterFavorite);
      userCharacterFavoriteRepository.save(userCharacterFavoriteEntity);
      return ResponseDto.builder().message(Constant.OK).build();
    } else {
      throw new AdmCharacterException("Character already exists in favorites",
          HttpStatus.BAD_REQUEST, null, null);
    }
  }

  @Override
  public ResponseDto updateCharactersFavorite(Integer id,
      UserCharacterFavoriteDto userCharacterFavorite) {
    UserCharacterFavorite userCharacterFavoriteEntity = userCharacterFavoriteRepository.findById(id)
        .get();
    userCharacterFavoriteEntity.setStatus(userCharacterFavorite.getStatus());
    userCharacterFavoriteRepository.save(userCharacterFavoriteEntity);
    return ResponseDto.builder().message(Constant.OK).build();
  }

  @Override
  public ResponseDto getCharactersFavoriteByIdUser(Integer idUser) {
    List<UserCharacterFavorite> userCharacterFavoriteList = userCharacterFavoriteRepository.findByIdUserAndStatus(
        idUser, Boolean.TRUE);
    if (Objects.isNull(userCharacterFavoriteList)) {
      throw new AdmCharacterException("characters not exists", HttpStatus.NOT_FOUND, null, null);
    } else {
      return ResponseDto.builder().message("ok").userCharacterFavoriteList(
          UserCharacterFavoriteMapper.MAPPER.mapUserCharacterFavoriteListToUserCharacterFavoriteDtoList(
              userCharacterFavoriteList)).build();
    }
  }
}
