package com.adm.character.service.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import lombok.AccessLevel;

@lombok.Data
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Builder
@lombok.experimental.FieldDefaults(level = AccessLevel.PRIVATE)
public class UserCharacterFavoriteDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer id;

  @Valid
  @NotNull(message = "user not be null")
  private UserDto user;

  @NotNull(message = "idCharacter not be null")
  private Integer idCharacter;

  @NotEmpty(message = "email not be null or empty")
  @Size(max = 5000, min = 1, message = "email min 1 and max 100.")
  private String character;

  @NotNull(message = "status not be null")
  private Boolean status;
}
