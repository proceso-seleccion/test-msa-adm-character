package com.adm.character.service.dto;

import java.io.Serializable;
import java.util.List;
import lombok.AccessLevel;

@lombok.Data
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Builder
@lombok.experimental.FieldDefaults(level = AccessLevel.PRIVATE)
public class CharacterDto implements Serializable {

  private static final long serialVersionUID = 1L;

  List<ItemDto> items;


  @lombok.Getter
  @lombok.Setter
  @lombok.experimental.FieldDefaults(level = AccessLevel.PRIVATE)
  public static class ItemDto {

    String id;
    String name;
    String ki;
    String maxKi;
    String race;
    String gender;
    String description;
    String image;
    String affiliation;
    String deletedAt;
    PlanetDto originPlanet;
  }

  @lombok.Getter
  @lombok.Setter
  @lombok.experimental.FieldDefaults(level = AccessLevel.PRIVATE)
  public static class PlanetDto {
    String name;
  }
}
