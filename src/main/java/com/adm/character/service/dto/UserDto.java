package com.adm.character.service.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import lombok.AccessLevel;

@lombok.Data
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Builder
@lombok.experimental.FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto implements Serializable {

  private static final long serialVersionUID = 1L;

  Integer id;

  @NotEmpty(message = "name not be null or empty")
  @Size(max = 30, min = 1, message = "name min 1 and max 100.")
  String name;

  @NotEmpty(message = "email not be null or empty")
  @Size(max = 100, min = 1, message = "email min 1 and max 100.")
  @Email(message = "email not valid")
  String email;

  @NotEmpty(message = "password not be null or empty")
  @Size(max = 600, min = 1, message = "password min 1 and max 600.")
  String password;

  @NotNull(message = "status not be null")
  Boolean status;
}
