package com.adm.character.service.dto;

import java.util.List;
import lombok.AccessLevel;
import lombok.Builder;

@lombok.Data
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@Builder
@lombok.experimental.FieldDefaults(level = AccessLevel.PRIVATE)
public class ResponseDto {

  String message;
  UserDto user;
  List<UserCharacterFavoriteDto> userCharacterFavoriteList;
}
