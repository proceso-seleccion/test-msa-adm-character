package com.adm.character.service;

import com.adm.character.service.dto.ResponseDto;
import com.adm.character.service.dto.UserCharacterFavoriteDto;
import com.adm.character.service.dto.UserDto;
import feign.Response;

public interface UserService {

  ResponseDto save(UserDto user);
  ResponseDto getByEmailPassword(String email, String password);
  ResponseDto saveCharactersFavorite(UserCharacterFavoriteDto userCharacterFavorite);
  ResponseDto updateCharactersFavorite(Integer id,UserCharacterFavoriteDto userCharacterFavorite);
  ResponseDto getCharactersFavoriteByIdUser(Integer idUser);

}
