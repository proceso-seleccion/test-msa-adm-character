package com.adm.character.service.mapper;

import com.adm.character.domain.UserCharacterFavorite;
import com.adm.character.service.dto.UserCharacterFavoriteDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserCharacterFavoriteMapper {

  UserCharacterFavoriteMapper MAPPER = Mappers.getMapper(UserCharacterFavoriteMapper.class);

  UserCharacterFavorite mapUserCharacterFavoriteDtoToUserCharacterFavorite(
      UserCharacterFavoriteDto userCharacterFavorite);

  List<UserCharacterFavoriteDto> mapUserCharacterFavoriteListToUserCharacterFavoriteDtoList(
      List<UserCharacterFavorite> userCharacterFavoriteList);
}
