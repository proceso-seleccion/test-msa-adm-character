package com.adm.character.service.mapper;

import com.adm.character.domain.User;
import com.adm.character.service.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {
  UserMapper MAPPER = Mappers.getMapper(UserMapper.class);
  User mapUserDtoToUser(UserDto user);
  UserDto mapUserToUserDto(User user);
}
