package com.adm.character.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users_character_favorite")
public class UserCharacterFavorite extends BaseEntity {

  @ManyToOne
  @JoinColumn(name = "id_user", nullable = false)
  private User user;

  @Column(name = "id_user", insertable = false, updatable = false)
  private Integer idUser;

  @NotNull(message = "not be null")
  @Column(name = "id_character", nullable = false)
  private Integer idCharacter;

  @NotEmpty(message = "email not be null or empty")
  @Size(max = 5000, min = 1, message = "email min 1 and max 100.")
  @Column(name = "character", length = 5000, nullable = false)
  private String character;
}
