package com.adm.character.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@NoArgsConstructor
@lombok.experimental.FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Table(name = "users")
public class User extends BaseEntity {

  @NotEmpty(message = "name not be null or empty")
  @Size(max = 30, min = 1, message = "name min 1 and max 100.")
  @Column(name = "name", length = 100, nullable = false)
  String name;

  @NotEmpty(message = "email not be null or empty")
  @Size(max = 100, min = 1, message = "email min 1 and max 100.")
  @Column(name = "email", length = 100, nullable = false)
  String email;

  @NotEmpty(message = "password not be null or empty")
  @Size(max = 600, min = 1, message = "password min 1 and max 600.")
  @Column(name = "password", length = 600, nullable = false)
  String password;

}
