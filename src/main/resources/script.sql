CREATE SEQUENCE SEQ_USER START WITH 1 BELONGS_TO_TABLE;
CREATE SEQUENCE SEQ_USER_CHARACTER_FAVORITE START WITH 1 BELONGS_TO_TABLE;
CREATE CACHED TABLE users (
    id INTEGER DEFAULT NEXT VALUE FOR SEQ_USER NOT NULL NULL_TO_DEFAULT SEQUENCE SEQ_USER,
    name  VARCHAR(100) NOT NULL,
    email  VARCHAR(100) NOT NULL,
    password  VARCHAR(600) NOT NULL,
    status  BOOLEAN NOT NULL
);
ALTER TABLE users ADD CONSTRAINT "PUBLIC"."CONSTRAINT_1" PRIMARY KEY(id);

CREATE CACHED TABLE users_character_favorite(
    id INTEGER DEFAULT NEXT VALUE FOR SEQ_USER_CHARACTER_FAVORITE NOT NULL NULL_TO_DEFAULT SEQUENCE SEQ_USER_CHARACTER_FAVORITE,
    id_user INTEGER NOT NULL,
    id_character INTEGER NOT NULL,
    character  VARCHAR(5000) NOT NULL,
    status  BOOLEAN NOT NULL
);
ALTER TABLE users_character_favorite ADD CONSTRAINT "PUBLIC"."CONSTRAINT_2" PRIMARY KEY(id);
ALTER TABLE users_character_favorite ADD CONSTRAINT "PUBLIC"."FK_USER_CHARACTER_FAVORITE" FOREIGN KEY(id_user) REFERENCES users(id) NOCHECK;